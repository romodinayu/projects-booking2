package com.projects.booking.config;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

/**
 * Default timezone
 */
@Component
public class TimeZoneConfig {

    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
}