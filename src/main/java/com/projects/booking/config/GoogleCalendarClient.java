package com.projects.booking.config;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration {@link Calendar}
 */
@Configuration
@RequiredArgsConstructor
public class GoogleCalendarClient {

    private final GoogleCalendarProperties calendarConfig;
    private final JsonFactory jsonFactory = new JacksonFactory();

    @Bean
    public Calendar getGoogleCalendarClient() throws GeneralSecurityException, IOException {

        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        Credential credential = new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(jsonFactory)
                .setServiceAccountId(calendarConfig.getServiceAccountId())
                .setServiceAccountScopes(Collections.singleton(CalendarScopes.CALENDAR))
                .setServiceAccountPrivateKeyFromP12File(calendarConfig.getKeyFile())
                .build();

        return new Calendar
                .Builder(httpTransport, jsonFactory, credential)
                .setApplicationName(calendarConfig.getApplicationName())
                .build();
    }
}