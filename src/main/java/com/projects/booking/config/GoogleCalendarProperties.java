package com.projects.booking.config;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.File;

import lombok.Data;
import lombok.SneakyThrows;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "app.calendar")
public class GoogleCalendarProperties {

    @NotBlank
    private String applicationName;

    @NotBlank
    private String serviceAccountId;

    @NotBlank
    private String calendarId;

    @NotNull
    private File keyFile = getKey();

    @SneakyThrows
    private File getKey() {
        return new ClassPathResource("key.p12").getFile();
    }
}