package com.projects.booking.config;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import com.projects.booking.exception.CreateDirAvatarException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * GitHub-style random avatar generator
 */
@Slf4j
@Component
public class PixelAvatarGenerator {

    private static final byte AVATAR_PIXEL_SIZE = 5;

    private static final byte BORDER_IMG = 7;
    private static final byte SIZE_IMG = 100;
    private static final byte AVATAR_SIZE_INSIDE = SIZE_IMG - 2 * BORDER_IMG;

    private static final String AVATARS_PATH = System.getProperty("user.dir") + "/avatars/";
    private static final String DEFAULT_AVATAR = "avatar.png";

    /**
     * Changing the size of the avatar.
     * Adding white edging.
     *
     * @param pixelImg {@link BufferedImage}
     * @return img {@link BufferedImage}
     */
    private static BufferedImage resize(BufferedImage pixelImg) {
        Image tmpImg = pixelImg.getScaledInstance(AVATAR_SIZE_INSIDE, AVATAR_SIZE_INSIDE, 0);
        BufferedImage resized = new BufferedImage(SIZE_IMG, SIZE_IMG, BufferedImage.TYPE_INT_ARGB);
        Graphics2D avatar = resized.createGraphics();
        avatar.setBackground(Color.WHITE);
        avatar.clearRect(0, 0, SIZE_IMG, SIZE_IMG);
        avatar.drawImage(tmpImg, BORDER_IMG, BORDER_IMG, AVATAR_SIZE_INSIDE, AVATAR_SIZE_INSIDE, null);
        avatar.dispose();

        return resized;
    }

    /**
     * Getting an avatar for the user.
     * If the avatar does not exist, then a new one is created.
     *
     * @param username {@link String}
     * @return the name of the avatar file {@link String}
     */
    public String getAvatar(String username) {
        Assert.hasText(username, "username is blank");

        String avatarName = username + ".png";

        if (new File(AVATARS_PATH + avatarName).exists()) {
            return avatarName;
        }

        BufferedImage avatar = new BufferedImage(AVATAR_PIXEL_SIZE, AVATAR_PIXEL_SIZE, BufferedImage.TYPE_INT_ARGB);

        byte middleIndex = AVATAR_PIXEL_SIZE / 2;
        byte maxIndex = AVATAR_PIXEL_SIZE - 1;

        Random random = new Random();
        int randomColor = new Color(0, random.nextInt(255), random.nextInt(255)).getRGB();

        for (byte x = 0; x <= maxIndex; x++) {
            for (byte y = 0; y <= maxIndex; y++) {

                int color = (x <= middleIndex && random.nextBoolean()) || (x > middleIndex && avatar.getRGB(maxIndex - x, y) == randomColor)
                        ? randomColor
                        : Color.WHITE.getRGB();

                avatar.setRGB(x, y, color);
            }
        }

        try {
            log.info("Generated new avatar for the user: {}.", username);

            File dir = new File(AVATARS_PATH);
            if (!new File(AVATARS_PATH).exists() && !dir.mkdir()) {
                throw new CreateDirAvatarException("Error creating a directory for avatars");
            }

            ImageIO.write(resize(avatar), "png", new File(AVATARS_PATH + avatarName));

            return avatarName;
        } catch (IOException ex) {
            log.error("Avatar creation error.", ex);
        }

        return DEFAULT_AVATAR;
    }
}