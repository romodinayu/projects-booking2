package com.projects.booking.exception;

/**
 * When it was not possible to create a directory for saving avatars
 */
public class CreateDirAvatarException extends RuntimeException {

    public CreateDirAvatarException(String message) {
        super(message);
    }
}