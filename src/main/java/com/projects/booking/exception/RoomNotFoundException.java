package com.projects.booking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * If, when creating a new booking, the room is not found
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class RoomNotFoundException extends RuntimeException {

    public RoomNotFoundException(String message) {
        super(message);
    }
}
