package com.projects.booking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * If the booking is not found
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class BookingResourceNotFoundException extends RuntimeException {

    public BookingResourceNotFoundException(String message) {
        super(message);
    }
}