package com.projects.booking.repository;

import java.util.List;
import java.util.Optional;

import com.projects.booking.entity.Participant;
import com.projects.booking.entity.view.UserView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Access to the {@link Participant} table
 */
@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Long> {

    /**
     * Search for emails of participants
     *
     * @param request {@link String}
     * @return email's list {@link String}
     */
    @Query(
            value = "SELECT p.email " +
                    "FROM Participant p " +
                    "WHERE lower(p.login) LIKE (:request) OR lower(p.email) LIKE (:request)",
            nativeQuery = true
    )
    List<String> findEmails(@Param("request") String request);

    /**
     * Getting a user for a session
     *
     * @param login {@link String}
     * @return {@link Optional}
     */
    @Query(
            value = "SELECT p.login, p.password " +
                    "FROM Participant p " +
                    "WHERE p.login = (:login)",
            nativeQuery = true
    )
    Optional<UserView> findUser(@Param("login") String login);

    /**
     * Find participant by email
     *
     * @param email {@link String}
     * @return {@link Boolean}
     */
    Optional<Participant> findByEmail(String email);
}