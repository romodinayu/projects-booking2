package com.projects.booking.repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.projects.booking.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Access to the {@link Booking} table
 */
@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {

    /**
     * Search for bookings for the week
     *
     * @param beginDateTime {@link LocalDateTime}
     * @param endDateTime   {@link LocalDateTime}
     * @return {@link ArrayList} of {@link Booking}
     */
    @Query(
            value = "SELECT b " +
                    "FROM Booking b " +
                    "WHERE b.beginDateTime >= :beginDateTime AND b.beginDateTime < :endDateTime"
    )
    List<Booking> findBookingsWeek(
            @Param("beginDateTime") LocalDateTime beginDateTime,
            @Param("endDateTime") LocalDateTime endDateTime
    );

    /**
     * Checking the intersection of bookings
     *
     * @param beginDateTime {@link LocalDateTime}
     * @param endDateTime   {@link LocalDateTime}
     * @param roomId        {@link Long}
     * @return {@link Boolean}
     */
    @Query(
            value = "SELECT CASE WHEN count(b) > 0 THEN true ELSE false END " +
                    "FROM Booking b " +
                    "INNER JOIN b.room r " +
                    "WHERE r.id = :roomId " +
                    "AND b.beginDateTime < :endDateTime AND b.endDateTime > :beginDateTime"
    )
    boolean checkingIntersectionBookings(
            @Param("beginDateTime") LocalDateTime beginDateTime,
            @Param("endDateTime") LocalDateTime endDateTime,
            @Param("roomId") Long roomId
    );
}