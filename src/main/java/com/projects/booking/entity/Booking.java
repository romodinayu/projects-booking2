package com.projects.booking.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * The period of negotiations
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Booking implements Serializable {

    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Participants
     */
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "booking_participant",
            joinColumns = @JoinColumn(name = "booking_id"),
            inverseJoinColumns = @JoinColumn(name = "participant_id")
    )
    private Set<Participant> participants = new HashSet<>();

    /**
     * Room
     */
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "room_id")
    @ManyToOne(cascade = CascadeType.ALL)
    private Room room;

    /**
     * Date+time of the start of negotiations
     */
    @Column(nullable = false)
    private LocalDateTime beginDateTime;

    /**
     * Date+time of the end of negotiations
     */
    @Column(nullable = false)
    private LocalDateTime endDateTime;

    /**
     * Duration of negotiations
     */
    @Column(nullable = false, columnDefinition = "int2")
    private Integer duration;
}