package com.projects.booking.entity.view;

/**
 * User
 */
public interface UserView {

    /**
     * Username
     */
    String getLogin();

    /**
     * Encrypted password
     */
    String getPassword();
}