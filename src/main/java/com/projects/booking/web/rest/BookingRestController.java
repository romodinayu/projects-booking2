package com.projects.booking.web.rest;

import static com.projects.booking.web.rest.BookingRestController.BOOKING_PATH;
import com.projects.booking.service.booking.BookingService;
import com.projects.booking.web.rest.dto.response.BookingResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Getting booking data
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(BOOKING_PATH)
public class BookingRestController {

    public static final String BOOKING_PATH = "/booking";
    private static final String ID_PATH = "/{id}";

    private final BookingService bookingService;

    @GetMapping(ID_PATH)
    public BookingResponse getBooking(@PathVariable Long id) {
        return bookingService.getBooking(id);
    }
}