package com.projects.booking.service.booking.dto;

import java.util.List;

import lombok.Builder;
import lombok.Data;
import org.springframework.util.Assert;

@Data
@Builder
public class HourlyEventData {

    /**
     * The hour at which the event starts.
     * Example: 14:00
     */
    private final String hour;

    /**
     * List of bookings
     */
    private final List<List<EventData>> events;

    public static HourlyEventDataBuilder builder() {
        return new HourlyEventDataBuilder() {

            @Override
            public HourlyEventData build() {
                Assert.hasText(super.hour, "hour is blank");
                Assert.notNull(super.events, "events = null");

                return super.build();
            }
        };
    }

    @Data
    @Builder
    public static class EventData {

        /**
         * Booking ID
         */
        private final Long bookingId;

        /**
         * Name of room
         */
        private final String roomName;

        /**
         * List of participants
         */
        private final List<String> participants;

        public static EventDataBuilder builder() {
            return new EventDataBuilder() {

                @Override
                public EventData build() {
                    Assert.notNull(super.bookingId, "bookingId = null");
                    Assert.hasText(super.roomName, "roomName is blank");
                    Assert.notEmpty(super.participants, "participants is empty");

                    return super.build();
                }
            };
        }
    }
}