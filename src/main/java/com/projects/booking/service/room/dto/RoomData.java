package com.projects.booking.service.room.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.util.Assert;

/**
 * Room
 */
@Data
@Builder
public class RoomData {

    /**
     * ID
     */
    private final Long id;

    /**
     * Name
     */
    private final String name;

    public static RoomDataBuilder builder() {
        return new RoomDataBuilder() {

            @Override
            public RoomData build() {
                Assert.notNull(super.id, "id = null");
                Assert.hasText(super.name, "name is blank");

                return super.build();
            }
        };
    }
}
