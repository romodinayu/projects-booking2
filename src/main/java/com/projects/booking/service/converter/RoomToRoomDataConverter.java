package com.projects.booking.service.converter;

import com.projects.booking.entity.Room;
import com.projects.booking.service.room.dto.RoomData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * {@link Room} => {@link RoomData}
 */
@Component
public class RoomToRoomDataConverter implements Converter<Room, RoomData> {

    @Override
    public RoomData convert(Room entity) {

        return RoomData.builder()
                .id(entity.getId())
                .name(entity.getName())
                .build();
    }
}
