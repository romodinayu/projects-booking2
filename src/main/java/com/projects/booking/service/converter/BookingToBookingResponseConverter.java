package com.projects.booking.service.converter;

import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;

import com.projects.booking.entity.Booking;
import com.projects.booking.entity.Participant;
import com.projects.booking.web.rest.dto.response.BookingResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * {@link Booking} => {@link BookingResponse}
 */
@Component
public class BookingToBookingResponseConverter
        implements Converter<Booking, BookingResponse> {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    @Override
    public BookingResponse convert(final Booking entity) {
        return BookingResponse.builder()
                .id(entity.getId())
                .beginDateTime(formatter.format(entity.getBeginDateTime()))
                .endDateTime(formatter.format(entity.getEndDateTime()))
                .duration(entity.getDuration())
                .roomName(entity.getRoom().getName())
                .participants(
                        entity.getParticipants()
                                .stream()
                                .map(this::convertParticipant)
                                .collect(Collectors.toUnmodifiableList())
                )
                .build();
    }

    private BookingResponse.Participant convertParticipant(final Participant entity) {
        return BookingResponse.Participant.builder()
                .shortFullName(entity.getShortFullName())
                .email(entity.getEmail())
                .build();
    }
}
