package com.projects.booking.service.converter;

import java.time.Duration;

import com.projects.booking.service.booking.dto.CreateBookingData;
import com.projects.booking.web.dto.request.CreateBookingRequest;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * {@link CreateBookingRequest} => {@link CreateBookingData}
 */
@Component
public class CreateBookingRequestToCreateBookingDataConverter
        implements Converter<CreateBookingRequest, CreateBookingData> {

    @Override
    public CreateBookingData convert(final CreateBookingRequest request) {

        return CreateBookingData.builder()
                .roomId(request.getRoomId())
                .beginDateTime(request.getBeginDateTime())
                .endDateTime(request.getEndDateTime())
                .duration(getDuration(request))
                .participantsOfBooking(request.getParticipantsOfBooking())
                .build();
    }

    private Long getDuration(final CreateBookingRequest request) {
        return Duration.between(request.getBeginDateTime(), request.getEndDateTime()).toMinutes();
    }
}