package com.projects.booking.service.converter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.stream.Collectors;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.projects.booking.entity.Booking;
import com.projects.booking.entity.Participant;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * {@link Booking} => {@link Event}
 */
@Component
public class BookingToGoogleEventConverter
        implements Converter<Booking, Event> {

    @Override
    public Event convert(Booking booking) {
        final Event event = new Event();

        event.setSummary("Событие_" + booking.getId());
        event.setDescription(String.format("Бронирование переговорной комнаты '%s'", booking.getRoom().getName()));
        event.setStart(getEventDateTime(booking.getBeginDateTime()));
        event.setEnd(getEventDateTime(booking.getEndDateTime()));
        event.setAttendees(
                booking.getParticipants()
                        .stream()
                        .map(this::getEventAttendee)
                        .collect(Collectors.toUnmodifiableList())
        );

        return event;
    }

    /**
     * Converter {@link LocalDateTime} => {@link EventDateTime}
     *
     * @param dateTime {@link LocalDateTime}
     * @return {@link EventDateTime}
     */
    private EventDateTime getEventDateTime(final LocalDateTime dateTime) {
        final EventDateTime eventDateTime = new EventDateTime();
        eventDateTime.setDateTime(new DateTime(Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant())));
        eventDateTime.setTimeZone("Europe/Moscow");

        return eventDateTime;
    }

    /**
     * Converter {@link Participant} => {@link EventAttendee}
     *
     * @param participant {@link Participant}
     * @return {@link EventAttendee}
     */
    private EventAttendee getEventAttendee(final Participant participant) {
        final EventAttendee eventAttendee = new EventAttendee();
        eventAttendee.setDisplayName(participant.getFullName());
        eventAttendee.setEmail(participant.getEmail());

        return eventAttendee;
    }
}