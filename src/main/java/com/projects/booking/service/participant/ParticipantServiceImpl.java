package com.projects.booking.service.participant;

import java.util.List;

import com.projects.booking.repository.ParticipantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * Realization {@link ParticipantService}
 */
@Service
@RequiredArgsConstructor
public class ParticipantServiceImpl implements ParticipantService {

    private final ParticipantRepository participantRepository;

    @Override
    public List<String> searchEmails(final String request) {
        Assert.hasText(request, "request is blank");

        return participantRepository.findEmails(request + "%");
    }
}