package com.projects.booking.service.calendar;

import java.io.IOException;

import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.projects.booking.config.GoogleCalendarProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;
import org.springframework.util.Assert;

/**
 * Google-calendar service
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CalendarService {

    private final Calendar calendarClient;

    private final GoogleCalendarProperties calendarProperties;

    @TransactionalEventListener
    public void createEvent(final Event request) {
        Assert.notNull(request, "request = null");
        log.info("-> CalendarService.createEvent. Request: {}", request);

        try {
            final Event response = calendarClient
                    .events()
                    .insert(calendarProperties.getCalendarId(), request)
                    .execute();
            log.info("<- CalendarService.createEvent. Response: {}", response);
        } catch (IOException ex) {
            log.error("Error when integrating with the calendar.", ex);
        }
    }
}